import sys
import requests


def response(inputLat, inputLong):
    return requests.get(
        'https://nominatim.openstreetmap.org/reverse?format=json&lat=%s&lon=%s' % (inputLat, inputLong)).json()


def output(data):
    print('\n ********************* \n')
    print('PlaceId : ', data['place_id'])
    print('display_name : ', data['display_name'])
    print('Country : ', data['address']['country'])
    print('City : ', data['address']['state'])
    print('Full Address : ', data['address'])


if __name__ == '__main__':

    arg = sys.argv

    if len(arg) == 1:
        output(response(input(" enter latitude: "), input(" enter latitude: ")))
    else:
        output(response(arg[1].split('--lat=', 1)[1], arg[2].split('--long=', 1)[1]))
